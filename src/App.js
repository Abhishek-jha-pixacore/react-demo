import React from 'react';
// import logo from './logo.svg';
import './App.css';
import csk from './logo/csk.png';
import mi from './logo/mi.png';
import Vote from './Components/vote';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      cskVote: 0,
      miVote: 0
    }
  }
  cskVote = () => {
    this.setState({
      cskVote: this.state.cskVote+1
    })
  }
  miVote = () => {
    this.setState({
      miVote: this.state.miVote+1
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Vote csk={csk} cskVote={this.state.cskVote} cskVoteUp={this.cskVote}/>
          <Vote csk={mi} cskVote={this.state.miVote} cskVoteUp={this.miVote}/>
        </header>
      </div>
    );
  }
}

export default App;
